module.exports = {
    publicPath: './',
    configureWebpack: {
        output: {
            filename: 'index.bundle.js'
        },
        optimization: {
            splitChunks: false
        },
    },
    filenameHashing: false
}