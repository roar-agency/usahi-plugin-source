import Vue from 'vue'
import Vuex from 'vuex'
import counter from './modules/counter'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        screenWidth: window.innerWidth,
    },

    getters: {
       getScreenWidth(state) {
           return state.screenWidth;
       }
    },

    mutations: {
      changeWindowWidth(state, width) {
          state.screenWidth = width;
      }  
    },

    actions: {
       setWindowWidth(vuexContex, width) {
           vuexContex.commit('changeWindowWidth', width);
       }
    },
    modules: {
        counter
    }
});