import ApplicantFormPrimaryInfo from './components/Steps/PrimaryInfo/ApplicantFormPrimaryInfo'
import ApplicantFormMembersInfo from './components/Steps/MembersInfo/ApplicantFormMembersInfo'
import CompareAndShop from './components/Steps/CompareAndShop/CompareAndShop'
import Cart from './components/Steps/Cart/Cart'

export const routes = [
    { path: '', component: ApplicantFormPrimaryInfo },
    { path: '/second', component: ApplicantFormMembersInfo },
    { path: '/third', component: CompareAndShop },
    { path: '/fourth', component: Cart }
];