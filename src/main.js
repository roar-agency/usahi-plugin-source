import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import { store } from './store/store'

// Vue Material Components
import { MdButton, MdTable, MdContent, MdIcon, MdTabs, MdDialog, MdCheckbox, MdList } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'

//Custom Components
import AppInput from './components/Base/AppInput.vue'
import AppRadioInput from './components/Base/AppRadioInput.vue'

// Custom CSS file
import './assets/css/main.scss'

// Routes settings
import VueRouter from 'vue-router'
import { routes } from './router'

// Form Validation
import Vuelidate from 'vuelidate'

Vue.config.productionTip = false

// Form Validation
Vue.use(Vuelidate);

Vue.use(VueRouter);
const router = new VueRouter({
  routes,
});

/** Axios plugin settings */
Vue.use({
  install (Vue) {
    Vue.prototype.$axios = axios.create({
      baseURL: process.env.VUE_APP_API_BASE_URL
    })
  }
});

/** Vue Material components import*/
Vue.use(MdButton);
Vue.use(MdTable);
Vue.use(MdContent);
Vue.use(MdIcon);
Vue.use(MdTabs);
Vue.use(MdDialog);
Vue.use(MdCheckbox);
Vue.use(MdList);



/** Custom components use */
Vue.component('AppInput', AppInput);
Vue.component('AppRadioInput', AppRadioInput);

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')